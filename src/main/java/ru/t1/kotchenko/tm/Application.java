package ru.t1.kotchenko.tm;

import ru.t1.kotchenko.tm.api.ICommandRepository;
import ru.t1.kotchenko.tm.constant.ArgumentConstant;
import ru.t1.kotchenko.tm.constant.TerminalConstant;
import ru.t1.kotchenko.tm.model.Command;
import ru.t1.kotchenko.tm.repository.CommandRepository;

import java.util.Scanner;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        parseArguments(args);
        parseCommands();
    }

    private static void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private static void parseCommands() {
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.next();
            parseCommand(command);
        }
    }

    private static void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConstant.VERSION:
                showVersion();
                break;
            case TerminalConstant.HELP:
                showHelp();
                break;
            case TerminalConstant.INFO:
                showDeveloperInfo();
                break;
            case TerminalConstant.ARGUMENTS:
                showArguments();
                break;
            case TerminalConstant.COMMANDS:
                showCommands();
                break;
            case TerminalConstant.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported.");
    }

    private static void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            case ArgumentConstant.INFO:
                showDeveloperInfo();
                break;
            case ArgumentConstant.ARGUMENTS:
                showArguments();
                break;
            case ArgumentConstant.COMMANDS:
                showCommands();
                break;
            default:
                showArgumentError();
        }
        exit();
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported.");
        System.exit(1);
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        for (Command command : COMMAND_REPOSITORY.getTerminalCommand()) {
            System.out.println(command);
        }
    }

    public static void showArguments() {
        System.out.println("[ARGUMENTS]");
        for (Command command : COMMAND_REPOSITORY.getTerminalCommand()) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    public static void showCommands() {
        System.out.println("[COMMANDS]");
        for (Command command : COMMAND_REPOSITORY.getTerminalCommand()) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public static void showDeveloperInfo() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Ivan Kotchenko");
        System.out.println("E-MAIL: IvanKotchenko@yandex.ru");
    }

}